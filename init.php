<?php
// Place commands here which should execute after Boost
// has initialized, and before the app is determined/executed.

date_default_timezone_set('America/Los_Angeles');

if (preg_match("#live-site-url.com#", $_SERVER['SERVER_NAME'])) {
	define('IN_DEVELOPMENT', false);
} else {
	define('IN_DEVELOPMENT', true);
}

if (IN_DEVELOPMENT) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}