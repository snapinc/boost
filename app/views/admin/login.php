<div class="container login_form" id="login_form_container">
	<form action="admin/login" method="post" id="login_form">
		<label for="password">Enter Password:</label>
		<input type="password" name="password" class="form_input" id="login_form_password" />
		<button class="submit_button" id="login_form_submit">Login</button>
	</form>
</div>