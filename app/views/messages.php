<?php
	$errors = get_system_errors();
	$successes = get_system_successes();

	foreach( $errors as $error ):
?>
	<div class="system_error">
		<?= $error ?>
	</div>
<?php endforeach; ?>
<?php foreach( $successes as $success ): ?>
	<div class="system_success">
		<?= $success ?>
	</div>
<?php endforeach; ?>