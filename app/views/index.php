<div class="banners_container">

	<?php if (!empty($banners)): ?>
		<?php foreach($banners as $set_title => $set_contents):
			list( $subtitles, $paths, $thumbs ) = $set_contents;
		?>
			<div class="banner_set">
				<div class="banner_set_title"><?= $set_title ?></div>
				<div class="banner_set_contents">
					<?php foreach($subtitles as $i => $subtitle):
						$paired_path = $paths[$i];
						$paired_thumb = $thumbs[$i];

						preg_match('_([\d]+)x([\d]+)_', $paired_path, $matches);

						if (!empty($matches)) {
							list( $full, $width, $height ) = $matches;
						}

						if (!is_file( $paired_path )) continue;
					?>
						<div class="banner" data-framewidth="<?= $width ?>" data-frameheight="<?= $height ?>"  data-framesrc="<?= $paired_path ?>">
							<img src="<?= $paired_thumb ?>" class="banner_thumbnail">
							<div class="banner_title">
								<?= $subtitle ?>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="clearfix"></div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="system_error">
			No banners found.
		</div>
	<?php endif; ?>

</div>