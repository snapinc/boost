<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Banner Ads</title>
	<?php
		boost()->css->output();
	?>
</head>
<body>
	<div class="master_container">
		<div class="messages_container">
			<?php boost()->view->render('messages') ?>
		</div>
		<div class="contents_container">
			<?= $contents ?>
		</div>
	</div><!-- .master_container -->
	<?php
		boost()->js->output();
	?>
</body>
</html>