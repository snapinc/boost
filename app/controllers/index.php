<?php
boost()->route->add('/', function() {

	$banners = get_banner_directory_contents();

	boost()->template->render('default', 'index', array('banners' => $banners));

});
