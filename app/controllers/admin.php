<?php
boost()->route->group('admin', function() {

	boost()->route->add('?$:GET', function() {

		redirect('admin/login');

	});

	boost()->route->add('login', function() {

		$salt = '1545a3edd206bfb0a2c23f140d104e0d';
		$check_hash = '35302b81ca54b616a2f01de8588ea2e50a3113b0d112afb7488a3eaa72f86ef8';

		$post_vals = boost()->input->post();

		if (!empty($post_vals)) {

			$post_vals = sanitize_array( $post_vals );

			$password_to_hash = !empty($post_vals['password']) ? $post_vals['password'] : '';

			$password_hashed = hash('sha256', $password_to_hash . $salt );

			unset($password_to_hash);

			if ($password_hashed != $check_hash) {

				add_system_error('Incorrect Password');

			} else {

				set_logged_in();

				redirect( 'admin/dashboard' );
			}

		}

		boost()->template->render('default', 'admin/login');

	});

	boost()->route->add('dashboard', function() {



	});

});
