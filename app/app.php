<?php
boost()->asset->load('https://code.jquery.com/jquery-1.11.1.min.js');

boost()->js->load('dist/vendor.min.js');
boost()->css->load('dist/vendor.min.css');

boost()->js->load('dist/local.min.js');
boost()->css->load('dist/local.min.css');

boost()->utility->include_file_or_folder(boost()->get_app_path().'/controllers');

include_once( 'helpers.php' );