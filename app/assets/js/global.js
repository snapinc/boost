function Lightbox( element ) {

	var $element = $(element);

	var frame_width = parseInt( $element.attr('data-framewidth'), 10);
	var frame_height = parseInt( $element.attr('data-frameheight'), 10);
	var frame_src = $element.attr('data-framesrc');

	var $lbox;

	if (!frame_width || !frame_height || !frame_src) {
		throw "Could not find iframe stats.";
	}

	function show_lightbox() {
		$lbox = $('<div class="lightbox">');
		var $iframe = $('<iframe>').attr('src', frame_src).attr('width', frame_width + 'px').attr('height', frame_height + 'px').attr('class', 'lightbox_iframe').css({
			marginLeft : '-' + (frame_width / 2) + 'px',
			marginTop : '-' + (frame_height / 2) + 'px'
		});

		$lbox.append( $iframe );

		$('body').append( $lbox );

		$lbox.click( close_lightbox );
	}

	function close_lightbox(e) {

		e.stopPropagation();

		if ($lbox) {
			$lbox.remove();
		}

	}

	$element.click( show_lightbox );

}

$(document).ready(function() {
	$('.banner').each(function(i,e) {
		var lb = new Lightbox(e);
	});
});