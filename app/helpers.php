<?php

function sanitize_array( $input = array() ) {

	foreach( $input as $k => $v) {
		$input[$k] = sanitize_input( $v );
	}

	return $input;

}

function sanitize_input( $input = null ) {

	return strtolower( trim( strip_tags( $input ) ) );

}

function printout( $e ) {

	boost()->utility->printout( $e );

}

function debug( $e ) {
	printout($e);
	exit;
}

function redirect( $location = null, $code = 302 ) {

	header('Location: ' . $location, FALSE, $code );
	exit;

}

function is_logged_in() {

	$flag_set = boost()->session->get('logged_in', null);

	$expire_time = boost()->session->get('login_expire_time', 0);

	if ($expire_time < time()) {

		set_logged_in();

		redirect( 'admin/login' );
	}

	return $flag_set;

}

function set_logged_in() {

	boost()->session->set( 'logged_in' , true );
	boost()->session->set( 'login_expire_time', strtotime( '+1day', time() ) );

}

function set_logged_out() {

	boost()->session->set( 'logged_in', null );
	boost()->session->set( 'login_expire_time', 0 );

}

function add_system_message( $message, $level = 'success' ) {

	$messages = get_system_messages( $level );

	array_push( $messages, $message );

	boost()->value->set( $level, $messages );

}

function get_system_messages( $level = 'success' ) {

	$messages = boost()->value->get( $level );

	if (!$messages) {
		$messages = array();
	}

	return $messages;

}

function add_system_error( $message ) {

	add_system_message( $message, 'error' );

}

function get_system_errors() {

	return get_system_messages( 'error' );

}

function add_system_success( $message ){

	add_system_message( $message, 'success' );

}

function get_system_successes() {

	return get_system_messages( 'success' );

}

function sanitize_folder_name( $folder_name = null ) {

	return preg_replace('#\ -\\/#', '_', trim( $folder_name ) );

}

function asset_path( $url = null, $type = 'relative' ) {

	return boost()->url->asset_url( $url, $type );

}

function site_url($url = null, $type = 'relative', $add_base = true) {

	return boost()->url->site_url($url , $type, $add_base);

}

function banner_path( $banner_title = null, $banner_folder = null ) {

	$banner_path = '/banners';

	if (!empty($banner_title)) {
		$banner_title = strtolower( sanitize_folder_name( $banner_title ) );

		$banner_path .= '/' . $banner_title;
	}

	if (!empty($banner_folder)) {
		$banner_folder = sanitize_folder_name( $banner_folder );

		$banner_path .= '/' . $banner_folder;
	}

	$banner_path = trim( $banner_path, '/');

	return $banner_path;

}

function get_directory_contents( $path, $parent_name = false ) {

	$path = rtrim( $path, '/' );

	$titles = array();
	$contents = array();
	$filenames = array();

	$read_error = false;

	$open_dir =  dir( $path );

	if (!$open_dir) {

		add_system_error('Could not open directory: '. $path );
		$read_error = true;

	} else {

		while (( $entry = $open_dir->read()) !== false) {

			if ($entry == '.' || $entry == '..' || !is_dir( $path . '/' . $entry ) ) {
				continue;
			}


			$friendly_title = ucwords( str_replace('_', ' ', $entry ) );

			$path_to_use = $path . '/' . $entry;
			$filename = $entry . '.html';
			$thumb = $entry . '.png';

			if ($parent_name) {
				$parent_name = str_replace(' ', '_', $parent_name);
				$path_to_use = banner_path( $parent_name, $entry ) . '/' . $filename;
				$thumb = banner_path( $parent_name, $entry ) . '/assets/' . $thumb;
			}

			array_push($filenames, $thumb);
			array_push($titles, $friendly_title);
			array_push($contents, $path_to_use);

		}

		$open_dir->close();

	}

	$values = array( $titles, $contents, $filenames );

	if ($read_error) {
		$values = false;
	}

	return $values;

}

function get_banner_directory_contents() {

	$results = array();


	$read = get_directory_contents( banner_path() );

	if ($read) {

		list( $path_names, $absolute_paths ) = $read;

		foreach($absolute_paths as $k => $new_path) {

			$read = get_directory_contents( $new_path, $path_names[ $k ] );

			if ($read) {

				list( $new_names, $new_paths, $filenames ) = $read;

				$results[ $path_names[ $k ] ] = array(
					$new_names,
					$new_paths,
					$filenames
				);

				unset( $new_names );
				unset( $new_paths );

			}

		}

	} else {
		$results = false;
	}

	ksort($results);

	return $results;

}