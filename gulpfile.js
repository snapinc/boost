// ------------------- //
// -- CONFIG VALUES -- //
// ------------------- //

// IF TRUE, COMPUTER WILL BEEP WHEN AN ERROR IS DETECTED
var beep_on_error = true;

// IF TRUE, CONSOLE WILL LOG ERRORS WHEN DETECTED
var log_on_error = true;

// ---------------------- //
// -- WORK IN PROGRESS -- //
// ---------------------- //

// IF SET TO TRUE, all.min.css and all.min.js FILES WILL BE GENERATED
// IN THE DIST FOLDER, WHICH WILL CONTAIN CONTENTS FROM ALL LOCAL AND VENDOR
// CODE FOR BOTH CSS AND JS, RESPECTIVELY.
var combine_vendor_and_local = false;

// ----------------------------------- //
// -- DONT EDIT ANYTHING BELOW HERE -- //
// ----------------------------------- //

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var prefix = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var clean = require('gulp-clean');

if (beep_on_error || log_on_error) {
	var gutil = require('gulp-util');
}

var errorHandler = function (err) {
	if (beep_on_error) {
		gutil.beep();
	}
	if (log_on_error) {
		gutil.log(err);
	}
};

gulp.task('scss', function() {
	gulp.src('app/assets/css/**/*.css').pipe(clean());
	gulp.src('app/assets/scss/**/*.scss')
		.pipe(sass({onError:errorHandler}))
		.pipe(prefix("last 3 versions", "> 1%", "ie 8", "ie 9"))
		.pipe(gulp.dest('app/assets/css'));
	return;
});

gulp.task('local_css', ['scss'], function() {
	gulp.src('app/assets/dist/local.min.css').pipe(clean());
	gulp.src(['app/assets/css/global.css', 'app/assets/css/**/*.css'])
		.pipe(concat('local.min.css'))
		.pipe(minify())
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('local_js', function() {
	gulp.src('app/assets/dist/local.min.js').pipe(clean());
	gulp.src(['app/assets/js/global.js', 'app/assets/js/**/*.js'])
		.pipe(concat('local.min.js'))
		.pipe(replace(/\[SITE_URL\/?([^\]]+)?\]/gi, '/$1'))
		.pipe(uglify())
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('vendor_css', function() {
	gulp.src('app/assets/dist/vendor.min.css').pipe(clean());
	gulp.src('app/assets/vendor/css/**/*.css')
		.pipe(concat('vendor.min.css'))
		.pipe(minify())
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('vendor_js', function() {
	gulp.src('app/assets/dist/vendor.min.js').pipe(clean());
	gulp.src('app/assets/vendor/js/**/*.js')
		.pipe(concat('vendor.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('combine_js', function() {
	gulp.src('app/assets/dist/all.min.js').pipe(clean());
	gulp.src(['app/assets/dist/vendor.min.js', 'app/assets/dist/local.min.js'])
		.pipe(concat('all.min.js'))
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('combine_css', function() {
	gulp.src('app/assets/dist/all.min.css').pipe(clean());
	gulp.src(['app/assets/dist/vendor.min.css', 'app/assets/dist/local.min.css'])
		.pipe(concat('all.min.css'))
		.pipe(gulp.dest('app/assets/dist'));
	return;
});

gulp.task('watch', function() {
	watch('app/assets/vendor/js/**/*.js', function() {
		gulp.start('vendor_js');
	});

	watch('app/assets/vendor/css/**/*.css', function() {
		gulp.start('vendor_css');
	});

	watch('app/assets/js/**/*.js', function() {
		gulp.start('local_js');
	});

	watch('app/assets/scss/**/*.scss', function() {
		gulp.start('local_css');
	});

	if (combine_vendor_and_local) {
		watch('app/assets/dist/*min.js', function() {
			gulp.start('combine_js');
		});

		watch('app/assets/dist/*min.css', function() {
			gulp.start('combine_css');
		});
	}
});

// Default Task
gulp.task('default', [
	'vendor_css',
	'local_css',
	'vendor_js',
	'local_js',
	'watch'
]);