<?php
define('ROOTPATH', __dir__);

// DIRS
define('APPDIR', 'app');
define('TEMPLATEDIR', 'templates');
define('VIEWDIR', 'views');
define('ASSETDIR', 'assets');

// COMPOSER
require('vendor/autoload.php');

set_exception_handler('boost_exception_handler');

// INCLUDE INIT FILE (SO CUSTOM MODULES CAN BE LOADED, ETC)
@include(ROOTPATH.'/init.php');

boost()->init_callables();

// INCLUDE APP AUTOLOAD FILE IF EXISTS
@include(boost()->get_app_path().'/autoload.php');

boost()->hook->run('boost-app-before');

// START APP
@include(boost()->get_app_path().'/app.php');

boost()->hook->run('boost-route-run-before');

// RUN MATCHING ROUTES
boost()->route->determine_matching();
boost()->route->run_matching();

boost()->hook->run('boost-app-after');